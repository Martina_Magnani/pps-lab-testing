val scalatest = "org.scalatest" % "scalatest_2.12" % "3.0.1" % "test"

lazy val root = ( project in file (".")). settings (
  name := "pps-lab-testing", version := "0.1",
  organization := "unibo.pps", scalaVersion := "2.12.5",
  libraryDependencies ++= Seq ( scalatest ),
  coverageEnabled := true // always enabled
)