import org.scalatest.FunSuite
import u07lab._

class CartTest extends FunSuite{

  test("A Cart without products should has size = 0") {
      val cart:Cart = new BasicCart()
      assert(cart.size == 0)
  }

  test("A Cart with 2 products should has size = 2") {
    val cart:Cart = new BasicCart()
    cart.add(Item(Product("Acqua"), ItemDetails(1, Price(1.0))))
    cart.add(Item(Product("Panino"), ItemDetails(1, Price(4.50))))
    assert(cart.size == 2)
  }

  test("Cart contains items that we added") {
    val cart:Cart = new BasicCart()
    val acqua = Item(Product("Acqua"), ItemDetails(1, Price(1.0)))
    val panino = Item(Product("Panino"), ItemDetails(1, Price(4.50)))
    cart.add(acqua)
    cart.add(panino)

    assert(cart.content == Set(acqua,panino))
  }
}
